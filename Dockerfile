# Latest Ubuntu Image
FROM ubuntu:16.04

MAINTAINER chris dahl <chris@petshopboys-online.com>

USER root

# Get noninteractive frontend for Debian to avoid some problems:
#    debconf: unable to initialize frontend: Dialog
ENV DEBIAN_FRONTEND noninteractive

# Set terminal to xterm
ENV TERM="xterm"

# Install necessary programs
RUN apt-get update && apt-get install -y \
	apt-utils \
	zip \
	php \
	php-xml \
	php-mbstring \
	curl \
	phpunit \
	composer \
	vim

# Install security updates and patches
RUN apt-get update -q \
    && apt-get upgrade -y \
    && apt-get dist-upgrade -y \
    && apt-get autoclean -y \
	&& apt-get autoremove -y

# Adding history search with page up and page down
RUN sed -i'' 's|# "\\e\[5~": history-search-backward|"\\e\[5~": history-search-backward|' /etc/inputrc \
	&& sed -i'' 's|# "\\e\[6~": history-search-forward|"\\e\[6~": history-search-forward|' /etc/inputrc

# Following the Symfony3 install instructions
RUN mkdir -p /usr/local/bin \
	&& curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony \
	&& chmod a+x /usr/local/bin/symfony
	
# Create new Symfony Project
RUN mkdir /projects/ \
	&& symfony new /projects/StarWarsNames

WORKDIR /projects/StarWarsNames

# Removing Cache and Logs Dir (it's not needed - running the first time)
RUN rm -rf var/cache 

RUN composer install \
	&& composer require symfony/console \
	&& composer require chrisdahl/star-wars-names-bundle

CMD ["/bin/bash"]