# System Requirements for the StarWarsNames Symfony Bundle

Download and install the below listed programs/tools/scripts
* [PHP 7.0.x](http://php.net/downloads.php#v7.0.11)       
* [Symfony 3.1](http://symfony.com/download)
* [PHPUnit 5.6](https://phpunit.de/)
* [Console Support for Symfony](http://symfony.com/doc/current/components/console.html)
* Module php-xml for PHP (Example: `sudo apt-get install php-xml`)
* Optional: [Docker 1.12.2](https://www.docker.com/products/docker) - if installation via Docker is prefered 

# Symfony Bundle Installation

## Step 1a: Install the bundle via composer

    composer require chrisdahl/star-wars-names-bundle

## Step 1b: Install the project using Docker

* Build the attached Dockerfile for an image containing everything
    
    docker build Dockerfile

* Run the image with interactive terminal and continue with the next step (`vim` is pre-installed)

	docker run -it <image>

## Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

	<?php
	// app/AppKernel.php
	
	// ...
	class AppKernel extends Kernel
	{
	    public function registerBundles()
	    {
	        $bundles = array(
	            // ...
	            new Chrisdahl\StarWarsNamesBundle\ChrisdahlStarWarsNamesBundle(),
	        );
	        // ...
	    }	
	    // ...
	}

# Run

Open a console and change to your Symfony project root directory and then execute one of the following commands:

Get all the names: 

    php bin/console starwarsnames:all
	
Get a random name:
  
    php bin/console starwarsnames:random	
	
Get three random names:
 
    php bin/console starwarsnames:random 3


# Testing
    phpunit vendor/chrisdahl/star-wars-names-bundle/Tests/
