<?php 
namespace Chrisdahl\StarWarsNamesBundle\Utils;

use Symfony\Component\Yaml\Yaml;

class GetRandomItem
{
	protected $starWarsNames = [];
	
	public function __construct() 
	{
		// Load init names
		$namesFile = __DIR__ . '/../Resources/config/starwars-names.yml';
		$this->starWarsNames = Yaml::parse(file_get_contents($namesFile));
		$this->starWarsNames = array_unique($this->starWarsNames);
	}
	
	/**
	 * 
	 * @param int $number
	 * @return string|array
	 */
	public function random($number)
	{
		if ($number === null) {
			// fine, just return one
			return $this->getRandomItem();
		} else {
			$names = [];
			$total = count($this->starWarsNames);
			// don't return more than exist
			for ($i=0; $i < min($number, $total); $i++) {
				$names[] = $this->getRandomItem();
			}
		}		
		return $names;
	}	
	
	/**
	 * 
	 * @return array
	 */
	public function getThemAll():array 
	{
		// return them all
		return $this->starWarsNames;
	}
	
	/**
	 * 
	 * @return string
	 */
	protected function getRandomItem():string 
	{
		// Shuffle all the (remaining) names
		shuffle($this->starWarsNames);
		// Shift a name off the beginning of all of the names
		return array_shift($this->starWarsNames);
	}		
}
