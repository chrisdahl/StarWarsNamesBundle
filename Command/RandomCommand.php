<?php

namespace Chrisdahl\StarWarsNamesBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

use Chrisdahl\StarWarsNamesBundle\Utils\GetRandomItem;

class RandomCommand extends Command
{
	protected function configure()
	{
		$this
		// the name of the command (the part after "bin/console")
		->setName('starwarsnames:random')
	
		// the short description shown while running "php bin/console list"
		->setDescription('Get random Star Wars names')
			
		// define optional argument
		->addArgument('number', InputArgument::OPTIONAL, 'How many Star Wars names do you want?')
	
		// the full command description shown when running the command with
		// the "--help" option
		->setHelp("Get random Star Wars names")
		;
	}
	
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$number = $input->getArgument('number');
	
		$randomItem = new GetRandomItem();
		$names = $randomItem->random($number);
	
		if (is_array($names) === true) {
			$names = implode(', ', $names);
		}
	
		$output->writeln([
				$names
		]);
	}

}
