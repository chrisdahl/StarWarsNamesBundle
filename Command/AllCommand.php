<?php

namespace Chrisdahl\StarWarsNamesBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Chrisdahl\StarWarsNamesBundle\Utils\GetRandomItem;

class AllCommand extends Command
{
	
	protected function configure()
	{
		$this
		// the name of the command (the part after "bin/console")
		->setName('starwarsnames:all')
	
		// the short description shown while running "php bin/console list"
		->setDescription('Get all Star Wars names')
	
		// the full command description shown when running the command with
		// the "--help" option
		->setHelp("Get all Star Wars names")
		;
	}
	
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$randomItem = new GetRandomItem();
		$names = $randomItem->getThemAll();
	
		$output->writeln([
				implode(', ', $names)
		]);
	}	
}
