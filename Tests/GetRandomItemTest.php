<?php
use Chrisdahl\StarWarsNamesBundle\Utils\GetRandomItem;

class GetRandomItemTest extends \PHPUnit_Framework_TestCase
{
	public function testThatNoExceptionIsThrownWhenLoadingTheConfigFile()
	{
		// Test, that no Exceptions is thrown. Exception will be thrown if config file does not exist or yaml-syntax incorrect
		try {
			$randomItem = new GetRandomItem();
		} catch (\Exception $e) {
			$this->fail();
		}
	}

	public function testIfRandomWithNullParameterReturnsOneItemWhichIsAString()
	{
		// Test, if result is string
		$randomItem = new GetRandomItem();
		$item = $randomItem->random(null);

		$this->assertTrue(is_string($item));
	}

	public function testIfRandomReturnsOneItem()
	{
		// Test, if result is array with one item
		$randomItem = new GetRandomItem();
		$item = $randomItem->random(1);

		$this->assertCount(1, $item);
	}

	public function testIfRandomWithParameterThreeReturnsThreeItems()
	{
		// Test, if result is array with three items
		$randomItem = new GetRandomItem();
		$items = $randomItem->random(3);

		$this->assertCount(3, $items);
	}

	public function testReturnMaxNamesEvenIfRequestedNumberIsLarger()
	{
		// Test, if max result is max number of elements even if number is larger
		$randomItem = new GetRandomItem();
		$number = 734;
		$items = $randomItem->random($number);

		$this->assertLessThan($number, count($items));
	}

}